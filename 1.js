document.addEventListener("DOMContentLoaded", function() {
    let currentIndex = 0;
    let images = document.getElementsByClassName("image-to-show");
  
    function switchImage() {
      images[currentIndex].style.display = "none"; // Ховаємо поточне зображення
      currentIndex++;
      if (currentIndex >= images.length) {
        currentIndex = 0;
      }
      images[currentIndex].style.display = "block"; // Відображаємо нове зображення 
    }
  
    let intervalId = setInterval(switchImage, 3000);
    const buttonStop = document.createElement("button");
    buttonStop.textContent = "Припинити";
    buttonStop.style.backgroundColor = "red"; 
    buttonStop.style.color = "white"; 
    buttonStop.style.border = "1px solid black"; 
    buttonStop.style.width = "100px";
    buttonStop.style.height = "30px";
    buttonStop.style.cursor = "pointer";
    buttonStop.style.borderRadius = "5px";
   
    document.getElementById("wrapper").appendChild(buttonStop); // Добавляємо кнопку Припинити на екран
    buttonStop.addEventListener("click", function() {
    clearInterval(intervalId); // Зупиняємо інтервал при натисненні на кнопку
  });

    const buttonResume = document.createElement("button");
    buttonResume.textContent = "Відновити показ";
    buttonResume.style.backgroundColor = "aqua"; 
    buttonResume.style.color = "black"; 
    buttonResume.style.border = "1px solid black"; 
    buttonResume.style.width = "150px";
    buttonResume.style.height = "30px";
    buttonResume.style.cursor = "pointer";
    buttonResume.style.borderRadius = "5px";
   
    document.getElementById("wrapper").appendChild(buttonResume); // Добавляємо кнопку "Відновити показ" на екран
    buttonResume.addEventListener("click", function() {
    clearInterval(intervalId); // Зупиняємо поточний інтервал перед запуском нового
    intervalId = setInterval(switchImage, 3000); // Запускаємо новий інтервал
  });
  });
